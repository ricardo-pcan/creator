import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import promise from 'redux-promise-middleware';
import rootReducer from '../reducers/index';

const logger = createLogger();

export default function configureStore(initialState) {
  const store = createStore(rootReducer, applyMiddleware(thunk, logger, promise()));
  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextReducer = rootReducer;
      store.replaceReducer(nextReducer);
    });
  }
  return store;
}
