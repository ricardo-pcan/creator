import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as TodoActions from '../actions/index';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Dialog from 'material-ui/Dialog';

import SearchBox from '../components/SearchBox';
import Results from '../components/Results';
import Grid from '../components/Grid';
import Preview from '../components/Preview';

const style = {
  display: 'flex',
  justifyContent: 'flex-end'
}

class Planning extends Component {

  state = {
    open: false
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  handleSubmit = (values) => {
    const {actions, resources} = this.props;
    if(resources.type !== "desktop") {
      actions.fetchResources(resources.type, values.search);
    }
  }

  loadMore = () => {
    const {actions} = this.props;
    actions.loadMore();
  }

  updateTypeSearch = (type) => {
    this.props.actions.updateTypeSearch(type);
  }

  renderMoreResults() {
    const {resources} = this.props;
    if(resources.next) {
      return (
        <div>
          <button onClick={this.loadMore}>Cargar más</button>
        </div>
      );
    }
  }

  renderResults() {
    const {resources} = this.props;
    if(resources.loading) {
      return <div className="results"><img className="loading" src="assets/loading.gif" /></div>;
    } else {
      return (
        <Results results={resources.stream} type={resources.type}>
          {this.renderMoreResults()}
        </Results>);
    }
  }

  handleShowPreview = () => {
    const {actions} = this.props;
    actions.showPreview();
  }

  renderModals = () => {
    const {panel} = this.props;
    return (
      <div>
        {panel.showPreview ? <Preview/> : null}
      </div>
    );
  }

  render() {
    const {resources, actions} = this.props;
    return (
      <MuiThemeProvider>
        <div className="wrapper-planning">
          <div className="flex-planning">
            <div className="planning">
              <img src="../assets/creator.png" alt="logo-creator" className="img-logo" />
              <h1>MIS PLANEACIONES</h1>
              <h2>Título</h2>
              <input type="text" placeholder='ej. "La monarquía y absolutismo"' />
              <h2>Año escolar</h2>
              <select>
                <option>ej 1º Secundaria</option>
              </select>
              <h2>Materia</h2>
              <select>
                <option>ej. Matemáticas</option>
              </select>
              <div>
                <RaisedButton
                  className="filter-btn"
                  backgroundColor={'#777777'}
                  label="Filtrar planeación"
                  labelColor={'#FFF'}
                />
              </div>
            </div>
            <div className="planning-column">
              <div className="start-border">
                <div>
                  <img src="assets/img1.png"/>
                </div>
                <div>
                  <h1>Comienza a crear tus planeaciones</h1>
                  <p>¡Es fácil y práctico!</p>
                  <RaisedButton
                    className="new-btn"
                    label="Nueva planeación"
                    labelColor={'#FFF'}
                    backgroundColor={'#A53B8B'}
                    onTouchTap={this.handleOpen}
                  />
                  <Dialog
                    open={this.state.open}
                    onRequestClose={this.handleClose}
                  >
                    <div className="modal">
                      <div>
                        <button
                          className="close-modal"
                          onTouchTap={this.handleClose}
                        >x
                        </button>
                      </div>
                      <h1>¿Acerca de qué es tu planeación?</h1>
                      <h2>Elige un título</h2>
                      <input type="text" placeholder='ej. "La monarquía y absolutismo"' />
                      <h2>¿Para qué año escolar es?</h2>
                      <select>
                        <option>ej 1º Secundaria</option>
                      </select>
                      <h2>¿A qué materia pertenece?</h2>
                      <select>
                        <option>ej. Matemáticas</option>
                      </select>
                      <h2>Elige un grid</h2>
                      <div className="choose-grid">
                        <div className="unselected-grid">
                          <div className="grid-column">
                            <div className="four-grid"></div>
                            <div className="four-grid"></div>
                          </div>
                          <div className="grid-column">
                            <div className="four-grid"></div>
                            <div className="four-grid"></div>
                          </div>
                        </div>
                        <div className="selected-grid">
                          <div className="grid-column">
                            <div className="six-grid"></div>
                            <div className="six-grid"></div>
                          </div>
                          <div className="grid-column">
                            <div className="six-grid"></div>
                            <div className="six-grid"></div>
                          </div>
                          <div className="grid-column">
                            <div className="six-grid"></div>
                            <div className="six-grid"></div>
                          </div>
                        </div>
                        <div className="unselected-grid">
                          <div className="grid-column">
                            <div className="nine-grid"></div>
                            <div className="nine-grid"></div>
                            <div className="nine-grid"></div>
                          </div>
                          <div className="grid-column">
                            <div className="nine-grid"></div>
                            <div className="nine-grid"></div>
                            <div className="nine-grid"></div>
                          </div>                          
                          <div className="grid-column">
                            <div className="nine-grid"></div>
                            <div className="nine-grid"></div>
                            <div className="nine-grid"></div>
                          </div>
                        </div>
                      </div>
                      <div>
                        <RaisedButton
                          className="new-btn"
                          label="Crear nueva planeación"
                          labelStyle={{textTransform: 'camellcase'}}
                          labelColor={'#FFF'}
                          backgroundColor={'#A53B8B'}
                          onTouchTap={this.handleOpen}
                        />
                      </div>
                    </div>
                  </Dialog>
                </div>
              </div>
              <div className="img-row">
                <div>
                  <img src="assets/circle1.png"/>
                  <p>Encuentra tus recursos</p>
                </div>
                <div>
                  <img src="assets/circle2.png"/>
                  <p>Elige entre los resultados</p>
                </div>
                <div>
                  <img src="assets/circle3.png"/>
                  <p>Arrastra y suelta</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

Planning.propTypes = {
  resources: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    resources: state.resources,
    panel: state.panel
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(TodoActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Planning);
