import React, {PropTypes, Component} from 'react';
import classnames from 'classnames';
import {Field, reduxForm} from 'redux-form';

import RaisedButton from 'material-ui/RaisedButton';
import {TextField} from 'redux-form-material-ui';


class SearchBox extends Component {

  updateTypeSearch(type) {
    this.props.updateTypeSearch(type);
  }

  render() {
    const {handleSubmit} = this.props;
    return (
      <form onSubmit={handleSubmit}>
        <Field
          name="search"
          component={TextField}
          hintText="Buscar"
          onKeyDown={(event) => {
            if(event.keyCode === 13){
              event.preventDefault();
            }
          }}
          />
        <RaisedButton
          disabled={this.props.loading}
          type="submit"
          label="med"
          onClick={this.updateTypeSearch.bind(this, 'med')}
          />
        <RaisedButton
          disabled={this.props.loading}
          type="submit"
          icon={<i className="fa fa-youtube-play" style={{color: '#DF0101'}} aria-hidden="true"></i>}
          onClick={this.updateTypeSearch.bind(this, 'youtube')}
          />
        <RaisedButton
          disabled={this.props.loading}
          type="submit"
          icon={<i className="fa fa-google" style={{color: '#088A29'}} aria-hidden="true"></i>}
          onClick={this.updateTypeSearch.bind(this, 'google')}
          />
        <RaisedButton
          disabled={this.props.loading}
          type="submit"
          icon={<i className="fa fa-wikipedia-w" aria-hidden="true"></i>}
          onClick={this.updateTypeSearch.bind(this, 'wikipedia')}
          />
        <RaisedButton
          disabled={this.props.loading}
          type="submit"
          icon={<i className="fa fa-desktop" aria-hidden="true"></i>}
          onClick={this.updateTypeSearch.bind(this, 'desktop')}
          />
      </form>
    );
  }
}

SearchBox.propTypes = {
  searching: React.PropTypes.string,
  updateTypeSearch: React.PropTypes.func
};

SearchBox = reduxForm({
  form: 'search'
})(SearchBox);

export default SearchBox;
