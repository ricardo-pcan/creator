import React, {PropTypes, Component} from 'react';
import Dropzone from 'react-dropzone';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import RaisedButton from 'material-ui/RaisedButton';
import * as TodoActions from '../actions/index';

class Computer extends Component {

  state = {
    preview: null
  }

  onDrop = (files) => {
    const {actions} = this.props;
    actions.uploadFile(files);
    this.setState({
        preview: files[0].preview
    });
  }

  uploadFile = () => {
    const {actions} = this.props;
    actions.uploadImage(this.state.files);
  }

  render() {
    return (
      <div className="other">
        <h2>Archivos</h2>
        <div style={{display: (null !== this.state.preview) ? 'none' : ''}}>
          <Dropzone  onDrop={this.onDrop} multiple={false}>
            <p>Arrastra aquí tus archivos.</p>
          </Dropzone>
        </div>
        {this.state.preview === null ? '' : <img style={{maxWidth: "100%"}}src={this.state.preview}/>}
        <FileResult result={this.state.computer}/>
      </div>
    );
  }
}

Computer.propTypes = {
};

function mapStateToProps(state) {
  return {
    computer: state.computer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(TodoActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Computer);
