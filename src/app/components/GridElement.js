import React, {Component} from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import classnames from 'classnames';

const style = {};

export default class GridElement extends Component {

  constructor(props) {
    super(props);
    this.state = {
      colored: false
    };
  }

  allowDrop = (event) => {
    event.preventDefault();
    this.setState({colored: true});
  }

  disableDrop = (event) => {
    event.preventDefault();
    this.setState({colored: false});
  }

  handleDrop = (event) => {
    event.preventDefault();
    this.setState({colored: false});
    this.props.handleDrop(this.props.coords, event.dataTransfer.getData("text"));
  }

  handleModal = () => {
    this.props.handleModal(this.props.coords);
  }

  render() {
    let styles = null;

    if(this.state.colored) {
      styles = classnames('grid-elem', 'grid-elem-hover');
    } else {
      styles = classnames('grid-elem');
    }
    return (
      <div className={styles} onDrop={this.handleDrop} onDragOver={this.allowDrop} onDragLeave={this.disableDrop}>
        <div className="grid-default">
          <div className="drop-here">
            <i className="fa fa-chevron-circle-down" aria-hidden="true"></i>
            <div>Arrastra aquí tu recurso</div>
          </div>
          <div className="options">
            <span onClick={this.handleModal} style={{color: '#5F04B4', fontSize: 15, cursor: 'pointer'}}>
              Agrega aqui texto
            </span>
          </div>
        </div>
      </div>
    );
  }
}


GridElement.propTypes = {
  coords: React.PropTypes.object,
  handleDrop: React.PropTypes.func,
  handleModal: React.PropTypes.func
};
