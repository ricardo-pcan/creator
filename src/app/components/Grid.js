import React, {Component} from 'react';
import GridElement from './GridElement';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as Actions from '../actions/index';
import GridElementOccupied from './GridElementOccupied';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import {Card} from 'material-ui/Card';
import TinyMCE from 'react-tinymce';

class Grid extends Component {

  constructor(props) {
    super(props);
    this.state = {
      rows: 2,
      open: false,
      editorText: '',
      coords: null
    }
  }

  handleDrop = (coords, resource) => {
    const {actions} = this.props;
    const index = coords.row * 3 + coords.column;
    actions.addResource(index, JSON.parse(resource));
  }

  handleClose = () => {
    this.setState({open: false});
  }

  handleModal = (coords) => {
    this.setState({
      open: true,
      coords: coords
    });
  }

  handleEditorChange = (event) => {
    this.setState({
      editorText: event.target.getContent()
    });
  }

  handleAddText = () => {
    const {actions} = this.props;
    const index = this.state.coords.row * 3 + this.state.coords.column;

    actions.addTextResource(index, this.state.editorText);
    this.setState({
      open: false,
      coords: null,
      editorText: ''
    });
  }

  render() {
    const {resources} = this.props.resources;
    const renderedRows = [];

    const actions = [
      <FlatButton
        label="Cancelar"
        primary={true}
        onTouchTap={this.handleClose}
        />,
      <FlatButton
        label="Agregar"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.handleAddText}
        />
    ];

    let counter = 0;

    while(counter <= this.state.rows) {
      let columns = 0;
      const row = [];

      while(columns <= 2) {
        const currentResource = resources[counter * 3 + columns];
        if(Object.keys(currentResource).length === 0 || typeof currentResource === 'undefined'){
          row.push(<GridElement
            coords={{row: counter, column: columns}}
            handleDrop={this.handleDrop}
            handleModal={this.handleModal}
            key={columns}/>
          );
        } else {
          row.push(<GridElementOccupied resource={currentResource} coords={{row: counter, column: columns}} handleDrop={this.handleDrop} key={columns}/>);
        }
        columns++;
      }

      renderedRows.push(<div className="grid-row" key={counter}>{row}</div>);
      counter ++;
    }

    return (
      <div className="grid">
        <Card style={{padding: '20px'}}>
          <div style={{padding: '0 0 20px 10px'}}>
            <TextField
              floatingLabelText="Título de la planeación"
              floatingLabelStyle={{color: '#3a014c'}}
              />
          </div>
        {renderedRows}
        </Card>
        <Dialog
          open={this.state.open}
          modal={true}
          actions={actions}
          onRequestClose={this.handleClose}
          >
          <div style={{height: '500px'}}>
            <TinyMCE
              content=""
              config={{
                menubar: false,
                height: 400,
                plugins: [
                  'advlist autolink lists link image charmap print preview anchor',
                  'searchreplace visualblocks code fullscreen',
                  'insertdatetime media table contextmenu paste code'
                ],
                toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
              }}
              onChange={this.handleEditorChange}
              />
          </div>
        </Dialog>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    resources: state.resources
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Grid);
