import React, {PropTypes, Component} from 'react';

class GoogleResult extends Component {

  static propTypes = {
    result: PropTypes.object
  }

  createMarkup(markup) {
    return {__html: markup};
  }

  render() {
    const {result} = this.props;

    return (
      <div
        className="result"
        draggable="true"
        onDragStart={event => {
          const {result} = this.props;
          event.dataTransfer.setData("text", JSON.stringify(result));
        }}>
        <div className="result-body">
          <div className="result-google">
            <h4 dangerouslySetInnerHTML={this.createMarkup(result.htmlTitle)}></h4>
            <h5 dangerouslySetInnerHTML={this.createMarkup(result.htmlFormattedUrl)}></h5>
            <p dangerouslySetInnerHTML={this.createMarkup(result.htmlSnippet)}></p>
          </div>
        </div>
      </div>
    );
  }
}

class YoutubeResult extends Component {

  static propTypes = {
    result: PropTypes.object
  }

  createMarkup(markup) {
    return {__html: markup};
  }

  render() {
    let image = null;
    const {result} = this.props;
    if(result.pagemap && result.pagemap.cse_image){
      if(result.pagemap.cse_image.length > 0) {
        image = result.pagemap.cse_image[0].src;
      }
    }
    return (
      <div
        className="result result-youtube"
        draggable="true"
        onDragStart={event => {
          const {result} = this.props;
          event.dataTransfer.setData("text", JSON.stringify(result));
        }}>
        <div className="result-play">
          <i className="fa fa-play" aria-hidden="true"></i>
        </div>
        <img src={result.image}/>
      </div>
    );
  }
}

class MEDResult extends Component {

  static propTypes = {
    result: PropTypes.object
  }

  createMarkup(markup) {
    return {__html: markup};
  }

  render() {
    let image = null;
    const {result} = this.props;
    if(result && result.pagemap && result.pagemap.cse_image && result.pagemap.cse_image.length > 0) {
      image = result.pagemap.cse_image[0].src;
    }
    return (
      <div
        className="result"
        draggable="true"
        onDragStart={event => {
          const {result} = this.props;
          event.dataTransfer.setData("text", JSON.stringify(result));
        }}>
        <div className="result-body">
          <div className="result-img">
            <div>{image ? <img src={image}/> : null}</div>
            <span></span>
          </div>
          <div className="result-description">
            <strong dangerouslySetInnerHTML={this.createMarkup(result.htmlTitle)}></strong>
            <p dangerouslySetInnerHTML={this.createMarkup(result.htmlSnippet)}></p>
          </div>
        </div>
      </div>
    );
  }
}

class WikipediaResult extends Component {

  static propTypes = {
    result: PropTypes.object
  }

  createMarkup(markup) {
    return {__html: markup};
  }

  drag = (event) => {
    const {result} = this.props;
    event.dataTransfer.setData("text", result);
  }

  render() {
    const {result} = this.props;

    return (
      <div
        className="result"
        draggable="true"
        onDragStart={event => {
          const {result} = this.props;
          event.dataTransfer.setData("text", JSON.stringify(result));
        }}>
        <div className="result-body">
          <div className="result-google">
            <h4 dangerouslySetInnerHTML={this.createMarkup(result.htmlTitle)}></h4>
            <h5 dangerouslySetInnerHTML={this.createMarkup(result.htmlFormattedUrl)}></h5>
            <p dangerouslySetInnerHTML={this.createMarkup(result.htmlSnippet)}></p>
          </div>
        </div>
      </div>
    );
  }
}

class FileResult extends Component {

  static propTypes = {
    result: PropTypes.object
  }

  createMarkup(markup) {
    return {__html: markup};
  }

  drag = (event) => {
    const {result} = this.props;
    event.dataTransfer.setData("text", result);
  }

  render() {
    const {result} = this.props;

    return (
      <div
        className="result result-youtube"
        draggable="true"
        onDragStart={event => {
          const {result} = this.props;
          event.dataTransfer.setData("text", JSON.stringify(result));
        }}>
        <div className="result-play">
          <i className="fa fa-play" aria-hidden="true"></i>
        </div>
        <img src={image}/>
      </div>
    );
  }
}

export {
  GoogleResult, YoutubeResult, MEDResult, WikipediaResult, FileResult
}
