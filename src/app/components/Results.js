import React, {PropTypes, Component} from 'react';
import {GoogleResult, YoutubeResult, MEDResult, WikipediaResult, FileResult} from './itemResults';
import Computer from './Computer';

class Results extends Component {

  render() {
    let results = [];
    if(this.props.results){
      results = this.props.results.map((result, index) => {
        switch(this.props.type) {
          case 'google': {
            return <GoogleResult key={index} result={result}/>;
          }
          case 'youtube': {
            return <YoutubeResult key={index} result={result}/>;
          }
          case 'med': {
            return <MEDResult key={index} result={result}/>;
          }
          case 'wikipedia': {
            return <WikipediaResult key={index} result={result}/>;
          }
        }
      });
    }

    if (this.props.type === 'desktop') {
      results = <Computer />;
    }
    return (
      <div className="results">
        {results}
        {this.props.children}
      </div>
    );
  }
}

Results.propTypes = {
  results: React.PropTypes.array,
  type: React.PropTypes.string
};

export default Results;
