import {
  UPLOAD_FILE_FULFILLED
} from '../constants/ActionTypes';

const initialState = {
  path: '',
  extension: ''
};

export default function computer(state = initialState, action) {
  switch (action.type) {
    case UPLOAD_FILE_FULFILLED:
      return {
        path: `http://localhost:3000/${action.payload.data.path}`,
        extension: action.payload.data.extension
      };
    default:
      return state;
  }
}
