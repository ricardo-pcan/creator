import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';
import todos from './todos';
import resources from './resources';
import panel from './panel';
import computer from './computer';

const rootReducer = combineReducers({
  todos,
  resources,
  form: formReducer,
  panel,
  computer
});

export default rootReducer;
