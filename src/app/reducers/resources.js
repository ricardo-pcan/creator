import {
  CHANGE_TYPE_SEARCH,
  ADD_RESOURCE,
  ADD_TEXT_RESOURCE,
  SAVE_RESOURCE
} from '../constants/ActionTypes';

const initialState = {
  type: 'med',
  stream: [],
  resources: JSON.parse(localStorage.getItem('resources') || "[{},{},{},{},{},{},{},{},{}]"),
  loading: false,
  next: null
};

export default function resources(state = initialState, action) {
  switch (action.type) {

    case CHANGE_TYPE_SEARCH:
      return {
        ...state,
        type: action.typeSearch
      };

    case "FETCH_RESOURCES_PENDING":
      return {
        ...state,
        loading: true
      };

    case "FETCH_RESOURCES_FULFILLED":
      let next = null;
      if(action.payload.data.queries.nextPage && action.payload.data.queries.nextPage.length > 0) {
        next = action.payload.data.queries.nextPage[0];
      }
      return {
        ...state,
        loading: false,
        stream: action.payload.data.items,
        next: next
      };

    case ADD_RESOURCE:
      const resources = [...state.resources];
      resources[action.payload.index] = {
        resource: action.payload.resource,
        type: state.type
      };
      return {
        ...state,
        resources
      };

    case ADD_TEXT_RESOURCE:
      const listResources = [...state.resources];
      listResources[action.payload.index] = {
        resource: action.payload.resource,
        type: 'text'
      };
      return {
        ...state,
        resources: listResources
      };

    case SAVE_RESOURCE:
      const stateResources = state.resources;
      localStorage.setItem('resources', JSON.stringify(stateResources));
      return {
        ...state
      };

    default:
      return state;
  }
}
