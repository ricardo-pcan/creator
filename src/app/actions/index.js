import * as types from '../constants/ActionTypes';
import * as configs from '../lib/resources';
import axios from 'axios';

export function addTodo(text) {
  return {type: types.ADD_TODO, text};
}

export function deleteTodo(id) {
  return {type: types.DELETE_TODO, id};
}

export function editTodo(id, text) {
  return {type: types.EDIT_TODO, id, text};
}

export function completeTodo(id) {
  return {type: types.COMPLETE_TODO, id};
}

export function completeAll() {
  return {type: types.COMPLETE_ALL};
}

export function clearCompleted() {
  return {type: types.CLEAR_COMPLETED};
}

export function updateTypeSearch(type) {
  return {type: types.CHANGE_TYPE_SEARCH, typeSearch: type};
}

export function fetchResources(type, search) {
  const {url, params} = configs.getConfig(type, search);
  return dispatch => {
    dispatch({
      type: types.FETCH_RESOURCES,
      payload: axios.get(url, {params: params})
    });
  };
}

export function loadMore() {
  return (dispatch, getState) => {
    const state = getState();
    const {type, } = state.resources;
    dispatch({
      type: types.FETCH_RESOURCES,
      payload: axios.get(url)
    });
  };
}

export function addResource(index, resource) {
  return {type: types.ADD_RESOURCE, payload: {index,resource}};
}

export function addTextResource(index, resource) {
  return {type: types.ADD_TEXT_RESOURCE, payload: {index, resource}};
}

export function saveResource() {
  return {type: types.SAVE_RESOURCE};
}

export function showPreview() {
  return {type: types.SHOW_PREVIEW};
}

export function hidePreview() {
  return {type: types.HIDE_PREVIEW};
}

export function uploadFile(files) {
  return (dispatch) => {
    const data = new FormData();
    data.append('file', files[0]);
    dispatch({
      type: types.UPLOAD_FILE,
      payload: axios.post('http://localhost:3000/api/v1/media', data)
    });
  };
}
